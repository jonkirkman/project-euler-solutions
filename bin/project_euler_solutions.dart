library project_euler_solutions;

import 'dart:math' as Math;

main() {
    
    /**
     * Problem 1 - Multiples of 3 and 5
     *
     * Find the sum of all the multiples of 3 or 5 below 1000
     */
    var Problem1 = () {
        var total = 0;
        for ( var i = 1; i < 1000; i++ ) {
            if ( i % 3 == 0 || i % 5 == 0 ) {
                total += 1;
            }
        }
        return total;
    }();
    
    print( "Problem 1:  $Problem1" );


    /**
     * Problem 2 - Even Fibonacci numbers
     *
     * By considering the terms in the Fibonacci sequence whose values
     * do not exceed four million, find the sum of the even-valued terms
     */
    var Problem2 = () {
        var total = 0;
        var n = [0,1];
        
        while ( n[1] < 4000000 ) {
            var next = n.removeAt(0) + n.first;
            n.add( next );
            if ( next % 2 == 0 ) {
                total += next;
            }
        }
        return total;
    }();

    print( "Problem 2:  $Problem2" );


    /**
     * Problem 3 - Largest prime factor
     *
     * The prime factors of 13195 are 5, 7, 13 and 29.
     * What is the largest prime factor of the number 600851475143 ?
     */


    /**
     * Problem 4 - Largest palindrome product
     *
     * A palindromic number reads the same both ways. The largest palindrome 
     * made from the product of two 2-digit numbers is 9009 = 91 × 99.
     * Find the largest palindrome made from the product of two 3-digit numbers.
     */
    var Problem4 = () {
        var palindromes = [];
        var n = 999;
        while ( n > 99 ) {
            for ( var i = 999; i > 99; i-- ) {
                var p = (n * i).toString();
                if ( p.length % 2 == 0 ) {
                    if ( p == p.split('').reversed.join('') ) {
                        palindromes.add( int.parse(p) );
                    }
                }
            }
            n--;
        }
        palindromes.sort();
        return palindromes.last;
    }();

    print( "Problem 4:  $Problem4" );


    /*
     * Problem 16 - Power digit sum
     *
     * 215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
     * What is the sum of the digits of the number 21000?
     */
    var Problem16 = Math.pow(2, 1000)
        .toString()
        .split('')
        .map( (d) => int.parse(d) )
        .reduce( (a, b) => a + b );
    
    print( "Problem 16: $Problem16" );
}